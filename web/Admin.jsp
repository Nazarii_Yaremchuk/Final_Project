<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="tab.name"/></title>
    <link href="https://fonts.googleapis.com/css?family=ZCOOL+XiaoWei" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<button><a href="<%=request.getContextPath()%>/logout"><fmt:message key="logout"/></a></button>
<center>
    <h1><fmt:message key="nazar_present"/></h1>
    <h2>
        <a href="new"><fmt:message key="add.new.user"/></a>
        <br>
        <a href="list"><fmt:message key="list.all.users"/></a>

    </h2>
</center>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2><fmt:message key="list.all.users"/></h2></caption>
        <tr>
            <th>ID</th>
            <th><fmt:message key="full.name"/></th>
            <th><fmt:message key="email"/></th>
            <th><fmt:message key="password"/></th>
            <th><fmt:message key="date.of.birth"/></th>
            <th><fmt:message key="role"/></th>
        </tr>
        <c:forEach var="user" items="${listUser}">
            <tr>
                <td><c:out value="${user.id}"/></td>
                <td><c:out value="${user.fullName}"/></td>
                <td><c:out value="${user.email}"/></td>
                <td><c:out value="${user.password}"/></td>
                <td><c:out value="${user.dateOfBirth}"/></td>
                <td><c:out value="${user.role}"/></td>
                <td>
                    <a href="edit?id=<c:out value='${user.id}' />"><fmt:message key="edit"/></a>
                    <a href="delete?id=<c:out value='${user.id}' />"><fmt:message key="delete"/></a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>


<%--film table--%>
<center>
    <h2>
        <a href="newFilm"><fmt:message key="add.new.film"/></a>
        <br>
        <a href="listFilm"><fmt:message key="list.all.films"/></a>
    </h2>
</center>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2><fmt:message key="list.all.films"/></h2></caption>
        <tr>
            <th>ID</th>
            <th><fmt:message key="film.title"/></th>
            <th><fmt:message key="description"/></th>
            <th><fmt:message key="duration"/></th>
            <th><fmt:message key="genre"/></th>
            <th><fmt:message key="rating"/></th>
        </tr>
        <c:forEach var="film" items="${listFilm}">
            <tr>
                <td><c:out value="${film.id}"/></td>
                <td><c:out value='${film.filmTitle}'/></td>
                <td><c:out value='${film.description}'/></td>
                <td><c:out value='${film.duration}'/></td>
                <td><c:out value='${film.genre}'/></td>
                <td><c:out value='${film.rating}'/></td>
                <td>
                    <a href="editFilm?id=<c:out value='${film.id}' />"><fmt:message key="edit"/></a>
                    <a href="deleteFilm?id=<c:out value='${film.id}' />"><fmt:message key="delete"/></a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>


<%--actor table--%>
<center>
    <h2>
        <a href="newActor"><fmt:message key="add.new.actor"/></a>
        <br>
        <a href="listActor"><fmt:message key="list.all.actors"/></a>
    </h2>
</center>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2><fmt:message key="list.all.actors"/></h2></caption>
        <tr>
            <th>ID</th>
            <th><fmt:message key="full.name"/></th>
            <th><fmt:message key="date.of.birth"/></th>
            <th><fmt:message key="nationality"/></th>
        </tr>
        <c:forEach var="actor" items="${listActor}">
            <tr>
                <td><c:out value="${actor.id}"/></td>
                <td><c:out value='${actor.fullName}'/></td>
                <td><c:out value='${actor.dateOfBirth}'/></td>
                <td><c:out value='${actor.nationality}'/></td>
                <td>
                    <a href="editActor?id=<c:out value='${actor.id}' />"><fmt:message key="edit"/></a>
                    <a href="deleteActor?id=<c:out value='${actor.id}' />"><fmt:message key="delete"/></a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>

<div class="footer-copyright text-center py-3">
    <a href="?locale=en"><fmt:message key="label.button.en"/></a>
    <a href="?locale=ua"><fmt:message key="label.button.ua"/></a>
</div>

</body>
</html>
