<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Cinema</title>
    <link href="https://fonts.googleapis.com/css?family=ZCOOL+XiaoWei" rel="stylesheet">
    <style>
        <%@include file="/css/style.css" %>
    </style>
</head>
<body>
<div class="container">
    <div class="box">
        <img class="avatar" src="https://cdn.filestackcontent.com/xtnbNPESWeYrCPMOqtFQ">
        <h1>Login Account</h1>
        <form action="/login" method="post">
            <p>Email</p>
            <input type="text" placeholder="Email" name="email" required>
            <p>Password</p>
            <input type="password" placeholder="Password" name="password" required>
            <input type="submit" value="Login">


            <a href="registration.jsp">Create New Account</a>
        </form>
    </div>
</div>
</body>
</html>
