<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="com.cinema.model.User" %>

<% User user = (User) session.getAttribute("logUser");
    if (user == null) {
        response.sendRedirect("jsp/login.jsp");
    }
%>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Welcome Page</title>
</head>
<body>
<button><a href="<%=request.getContextPath()%>/logout">Logout</a></button>
<h1> Welcome, <%= user.getFullName() %>
</h1>
<h3>Your Email: <%= user.getEmail() %>
</h3>

<div align="center">

    <h1>Films</h1>

    <table border="1" cellpadding="5">
        <caption><h2><fmt:message key="list.all.films"/></h2></caption>
        <tr>
            <th>ID</th>
            <th><fmt:message key="film.title"/></th>
            <th><fmt:message key="description"/></th>
            <th><fmt:message key="duration"/></th>
            <th><fmt:message key="genre"/></th>
            <th><fmt:message key="rating"/></th>
        </tr>
        <c:forEach var="film" items="${listFilm}">
            <tr>
                <td><c:out value="${film.id}"/></td>
                <td><c:out value='${film.filmTitle}'/></td>
                <td><c:out value='${film.description}'/></td>
                <td><c:out value='${film.duration}'/></td>
                <td><c:out value='${film.genre}'/></td>
                <td><c:out value='${film.rating}'/></td>
            </tr>
        </c:forEach>
    </table>
</div>


</body>
</html>
