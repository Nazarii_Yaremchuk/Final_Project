<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${sessionScope.lang}">
<head>
    <title>Cinema</title>
    <link href="https://fonts.googleapis.com/css?family=ZCOOL+XiaoWei" rel="stylesheet">
    <style>
        <%@include file="/css/style.css" %>
    </style>
</head>
<body>
<div class="container">
    <div class="index">
        <h3><fmt:message key="nazar_presents"/></h3>
        <br/>

        <button><a href="registration.jsp">registration</a></button>
        <br>
        <br>
        <button><a href="login.jsp">login</a></button>
        <br/>

    </div>
</div>
<div class="footer-copyright text-center py-3">
    <a href="?locale=en"><fmt:message key="label.button.en"/></a>
    <a href="?locale=ua"><fmt:message key="label.button.ua"/></a>
</div>


</body>
</html>
