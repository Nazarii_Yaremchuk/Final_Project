package com.cinema.controller;

import com.cinema.model.User;
import com.cinema.model.UserRole;
import com.cinema.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


public class LoginServlet extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger(LoginServlet.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        UserService userService = new UserService();
        try {
            User user = userService.authenticateUser(email, password);
            String userRole = user.getRole();
            List<User> listUsers = userService.getListOfUsers();

            if (userRole.equals(UserRole.ADMIN.toString())) {
                LOG.info("Admin's Home");
                HttpSession session = request.getSession(); //Creating a session
                session.setAttribute("logUser", user);
                request.setAttribute("listUsers", listUsers);
                request.getRequestDispatcher("/jsp/admin.jsp").forward(request, response);
            } else if (userRole.equals(UserRole.USER.toString())) {
                LOG.info("User's Home");
                HttpSession session = request.getSession();
                session.setMaxInactiveInterval(10 * 60);
                session.setAttribute("logUser", user);
                response.sendRedirect("/jsp/welcome.jsp");
            } else {
                LOG.info("Error message = " + userRole);
                request.setAttribute("errMessage", userRole);
                request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
