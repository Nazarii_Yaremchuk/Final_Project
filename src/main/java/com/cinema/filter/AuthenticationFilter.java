package com.cinema.filter;

import com.cinema.model.Film;
import com.cinema.model.User;
import com.cinema.model.UserRole;
import com.cinema.service.FilmService;
import com.cinema.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class AuthenticationFilter implements Filter {
    private static final Logger LOG = LogManager.getLogger(AuthenticationFilter.class);
    private UserService userService;
    private FilmService filmService;

    public AuthenticationFilter() {
    }

    public void init(FilterConfig fConfig) {
        userService = new UserService();
        filmService = new FilmService();
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession(false);

        boolean isStartPage = httpRequest.getRequestURI().endsWith("/");
        boolean isLoginPage = httpRequest.getRequestURI().endsWith("/login");
        boolean isLogoutPage = httpRequest.getRequestURI().endsWith("/logout");
        boolean isRegistrationRequest = httpRequest.getRequestURI().endsWith("/registration");
        User loggedInUser = (User) session.getAttribute("logUser");
        boolean isLoggedIn = loggedInUser != null;
        boolean isJspPath = httpRequest.getRequestURI().endsWith(".jsp");

        if (isStartPage) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/jsp/index.jsp");
            dispatcher.forward(request, response);
        } else if (isLoginPage) {
            login(httpRequest, (HttpServletResponse) response);
        } else if (isLogoutPage) {
            logout(httpRequest, (HttpServletResponse) response);
        } else if (isRegistrationRequest) {
            registration(httpRequest, (HttpServletResponse) response);
        } else if (isJspPath) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/jsp/" + httpRequest.getRequestURI());
            dispatcher.forward(request, response);
        } else if (isLoggedIn) {
            chain.doFilter(request, response);
        } else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/jsp/login.jsp");
            dispatcher.forward(request, response);
        }
    }

    private void login(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        try {
            User user = userService.authenticateUser(email, password);
            String userRole = user.getRole();

            if (userRole.equals(UserRole.ADMIN.toString())) {
                LOG.info("Admin's Home");
                HttpSession session = request.getSession(); //Creating a session
                session.setAttribute("logUser", user);
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/admin");
                requestDispatcher.forward(request, response);
            } else if (userRole.equals(UserRole.USER.toString())) {
                LOG.info("User's Home");
                HttpSession session = request.getSession();
                session.setMaxInactiveInterval(10 * 60);
                session.setAttribute("logUser", user);
                List<Film> listFilm = filmService.getListOfFilm();
                session.setAttribute("listFilm", listFilm);
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/welcome.jsp");
                requestDispatcher.forward(request, response);
            } else {
                LOG.info("Error message = " + userRole);
                request.setAttribute("errMessage", userRole);
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/");
                requestDispatcher.forward(request, response);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
    }

    protected void logout(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(false); //Fetch session object
        if (session != null) { //If session is not null
            session.invalidate(); //removes all session attributes bound to the session
            request.setAttribute("errMessage", "You have logged out successfully");
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/login.jsp");
            requestDispatcher.forward(request, response);
            LOG.info("Logged out");
        }
    }

    private void registration(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        response.setContentType("text/html;charset=UTF-8");
        String fullName = request.getParameter("fullName");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String dateOfBirth = request.getParameter("dateOfBirth");
        //make user object
        User userModel = new User(fullName, email, password, dateOfBirth);

        if (userService.saveUser(userModel)) {
            HttpSession session = request.getSession();
            session.setAttribute("logUser", userService.getUser(email));
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/welcome.jsp");
            requestDispatcher.forward(request, response);
        } else {
            String errorMessage = "This user is already registered!";
            HttpSession regSession = request.getSession();
            regSession.setAttribute("RegError", errorMessage);
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/registration.jsp");
            requestDispatcher.forward(request, response);
        }
    }

    public void destroy() {
    }


}