package com.cinema.service;

import com.cinema.dao.FilmDao;
import com.cinema.dao.FilmDaoImpl;
import com.cinema.model.Film;

import java.util.List;

public class FilmService {
    private FilmDao filmDao;

    public FilmService() {
        this.filmDao = new FilmDaoImpl();
    }

    public Film getFilm(int id) {
        return filmDao.getFilm(id);
    }

    public List<Film> getListOfFilm() {
        return filmDao.getAllFilms();
    }

    public boolean createFilm(Film film) {
        return filmDao.createFilm(film);
    }

    public boolean deleteFilm(Film film) {
        return filmDao.deleteFilm(film);
    }

    public boolean updateFilm(Film film) {
        return filmDao.updateFilm(film);
    }


}
