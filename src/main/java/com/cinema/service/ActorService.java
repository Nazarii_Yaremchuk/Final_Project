package com.cinema.service;

import com.cinema.dao.ActorDao;
import com.cinema.dao.ActorDaoImpl;
import com.cinema.model.Actor;

import java.util.List;

public class ActorService {
    private ActorDao actorDao;

    public ActorService() {
        this.actorDao = new ActorDaoImpl();
    }

    public Actor getActor(int id) {
        return actorDao.getActor(id);
    }

    public List<Actor> getListOfActor() {
        return actorDao.getAllActors();
    }

    public boolean createActor(Actor actor) {
        return actorDao.createActor(actor);
    }

    public boolean deleteActor(Actor actor) {
        return actorDao.deleteActor(actor);
    }

    public boolean updateActor(Actor actor) {
        return actorDao.updateActor(actor);
    }


}
