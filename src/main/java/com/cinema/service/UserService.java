package com.cinema.service;

import com.cinema.dao.LoginDao;
import com.cinema.dao.LoginDaoImpl;
import com.cinema.dao.UserDao;
import com.cinema.dao.UserDaoImpl;
import com.cinema.model.User;

import java.util.List;

public class UserService {

    private LoginDao loginDao;
    private UserDao userDao;

    public UserService() {
        this.loginDao = new LoginDaoImpl();
        this.userDao = new UserDaoImpl();
    }

    public boolean saveUser(User user) {
        return loginDao.saveUser(user);
    }

    public User authenticateUser(String email, String password) {
        return loginDao.authenticateUser(email, password);
    }

    public User getUser(String email) {
        return userDao.getUser(email);
    }

    public User getUser(int id) {
        return userDao.getUser(id);
    }

    public List<User> getListOfUsers() {
        return userDao.getAllUsers();
    }


    public boolean createUser(User user) {
        return userDao.createUser(user);
    }

    public boolean deleteUser(User user) {
        return userDao.deleteUser(user);
    }

    public boolean updateUser(User user) {
        return userDao.updateUser(user);
    }


}
