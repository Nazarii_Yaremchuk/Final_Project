package com.cinema.dao;

import com.cinema.model.Film;
import com.cinema.util.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FilmDaoImpl implements FilmDao {
    private static final Logger LOG = LogManager.getLogger(FilmDaoImpl.class);

    private static final String FIND_FILM_BY_ID = "SELECT * FROM film WHERE id = ?";
    private static final String FIND_ALL_FILMS = "SELECT * FROM film;";
    private static final String ADD_FILM = "INSERT INTO film (film_title, description, duration, genre, rating) " +
            "VALUES (?, ?, ?, ?, ?)";
    private static final String DELETE_FILM = "DELETE FROM film WHERE id = ?";
    private static final String UPDATE_FILM = "UPDATE film SET film_title = ?, description = ?, duration = ? , " +
            "genre = ?, rating= ? WHERE id = ?";

    @Override
    public Film getFilm(int id) {
        Film film = null;

        Connection jdbcConnection = ConnectionManager.getConnection();
        PreparedStatement statement = null;
        try {

            statement = jdbcConnection.prepareStatement(FIND_FILM_BY_ID);

            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String filmTitle = resultSet.getString("film_title");
                String description = resultSet.getString("description");
                Float duration = resultSet.getFloat("duration");
                String genre = resultSet.getString("genre");
                Float rating = resultSet.getFloat("rating");

                film = new Film(id, filmTitle, description, duration, genre, rating);
            }

            resultSet.close();
            statement.close();
            System.out.println("Get FILM");
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return film;
    }


    @Override
    public List<Film> getAllFilms() {
        List<Film> listOfFilms = new ArrayList<>();
        Connection jdbcConnection = ConnectionManager.getConnection();
        try {
            Statement statement = jdbcConnection.createStatement();
            ResultSet rs = statement.executeQuery(FIND_ALL_FILMS);
            while (rs.next()) {
                Film film = new Film();

                film.setId(rs.getInt("id"));
                film.setFilmTitle(rs.getString("film_title"));
                film.setDescription(rs.getString("description"));
                film.setDuration(rs.getFloat("duration"));
                film.setGenre(rs.getString("genre"));
                film.setRating(rs.getFloat("rating"));

                listOfFilms.add(film);
            }
            statement.close();
            rs.close();
        } catch (Exception e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return listOfFilms;
    }

    @Override
    public boolean createFilm(Film film) {
        Connection jdbcConnection = ConnectionManager.getConnection();

        PreparedStatement statement = null;
        try {
            statement = jdbcConnection.prepareStatement(ADD_FILM);

            statement.setString(1, film.getFilmTitle());
            statement.setString(2, film.getDescription());
            statement.setFloat(3, film.getDuration());
            statement.setString(4, film.getGenre());
            statement.setFloat(5, film.getRating());

            boolean rowInserted = statement.executeUpdate() > 0;

            return rowInserted;
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return false;
    }

    @Override
    public boolean deleteFilm(Film film) {
        Connection jdbcConnection = ConnectionManager.getConnection();

        PreparedStatement statement = null;
        boolean rowDeleted = false;
        try {
            statement = jdbcConnection.prepareStatement(DELETE_FILM);

            statement.setInt(1, film.getId());

            rowDeleted = statement.executeUpdate() > 0;

        } catch (SQLException e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return rowDeleted;
    }

    @Override
    public boolean updateFilm(Film film) {
        Connection jdbcConnection = ConnectionManager.getConnection();

        PreparedStatement statement = null;
        boolean rowUpdated = false;
        try {
            statement = jdbcConnection.prepareStatement(UPDATE_FILM);

            statement.setString(1, film.getFilmTitle());
            statement.setString(2, film.getDescription());
            statement.setFloat(3, film.getDuration());
            statement.setString(4, film.getGenre());
            statement.setFloat(5, film.getRating());
            statement.setInt(6, film.getId());

            rowUpdated = statement.executeUpdate() > 0;
            System.out.println("Update film");
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return rowUpdated;
    }
}
