package com.cinema.dao;

import com.cinema.model.Actor;
import com.cinema.util.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ActorDaoImpl implements ActorDao {
    private static final Logger LOG = LogManager.getLogger(ActorDaoImpl.class);

    private static final String FIND_ACTOR_BY_ID = "SELECT * FROM actor WHERE id = ?";
    private static final String FIND_ALL_ACTORS = "SELECT * FROM actor;";
    private static final String ADD_ACTOR = "INSERT INTO actor (full_name, date_of_birth, nationality) " +
            "VALUES (?, ?, ?)";
    private static final String DELETE_ACTOR = "DELETE FROM actor WHERE id = ?";
    private static final String UPDATE_ACTOR = "UPDATE actor SET full_name = ?, date_of_birth = ?, nationality = ?" +
            "WHERE id = ?";

    @Override
    public Actor getActor(int id) {
        Actor actor = null;

        Connection jdbcConnection = ConnectionManager.getConnection();
        PreparedStatement statement = null;
        try {

            statement = jdbcConnection.prepareStatement(FIND_ACTOR_BY_ID);

            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {

                String fullName = resultSet.getString("full_name");
                String dateOfBirth = resultSet.getString("date_of_birth");
                String nationality = resultSet.getString("nationality");

                actor = new Actor(id, fullName, dateOfBirth, nationality);
            }

            resultSet.close();
            statement.close();
            System.out.println("Get ACTOR");
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return actor;
    }

    @Override
    public List<Actor> getAllActors() {
        List<Actor> listOfActors = new ArrayList<>();
        Connection jdbcConnection = ConnectionManager.getConnection();
        try {
            Statement statement = jdbcConnection.createStatement();
            ResultSet rs = statement.executeQuery(FIND_ALL_ACTORS);
            while (rs.next()) {
                Actor actor = new Actor();

                actor.setId(rs.getInt("id"));
                actor.setFullName(rs.getString("full_name"));
                actor.setDateOfBirth(rs.getString("date_of_birth"));
                actor.setNationality(rs.getString("nationality"));

                listOfActors.add(actor);
            }
            statement.close();
            rs.close();
        } catch (Exception e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return listOfActors;
    }

    @Override
    public boolean createActor(Actor actor) {
        Connection jdbcConnection = ConnectionManager.getConnection();

        PreparedStatement statement = null;
        try {
            statement = jdbcConnection.prepareStatement(ADD_ACTOR);

            statement.setString(1, actor.getFullName());
            statement.setString(2, actor.getDateOfBirth());
            statement.setString(3, actor.getNationality());

            boolean rowInserted = statement.executeUpdate() > 0;

            return rowInserted;
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return false;
    }

    @Override
    public boolean deleteActor(Actor actor) {
        Connection jdbcConnection = ConnectionManager.getConnection();

        PreparedStatement statement = null;
        boolean rowDeleted = false;
        try {
            statement = jdbcConnection.prepareStatement(DELETE_ACTOR);

            statement.setInt(1, actor.getId());

            rowDeleted = statement.executeUpdate() > 0;

        } catch (SQLException e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return rowDeleted;
    }

    @Override
    public boolean updateActor(Actor actor) {
        Connection jdbcConnection = ConnectionManager.getConnection();

        PreparedStatement statement = null;
        boolean rowUpdated = false;
        try {
            statement = jdbcConnection.prepareStatement(UPDATE_ACTOR);

            statement.setString(1, actor.getFullName());
            statement.setString(2, actor.getDateOfBirth());
            statement.setString(3, actor.getNationality());
            statement.setInt(4, actor.getId());

            rowUpdated = statement.executeUpdate() > 0;
            System.out.println("Update actor");
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        } finally {
            ConnectionManager.disconnect();
        }
        return rowUpdated;
    }
}
