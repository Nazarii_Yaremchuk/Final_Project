package com.cinema.model;

public enum UserRole {
    ADMIN,
    USER;

    @Override
    public String toString() {
        return name();
    }
}
